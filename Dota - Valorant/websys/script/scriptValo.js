const agentContainer = document.querySelector('.agent-container');
const prevButton = document.querySelector('.prev-button');
const nextButton = document.querySelector('.next-button');
const agentDetails = document.getElementById('agentDetails');
const closeAgentButton = document.querySelector('.close');
const searchInput = document.getElementById('searchInput');
const roleFilter = document.getElementById('roleFilter');
const agentsPerPage = 3;
let currentPage = 0;
let agentsData = [];
let filteredAgents = [];

// Function to fetch agent data from the API
async function fetchAgentsFromAPI() {
    try {
        const response = await fetch('https://valorant-api.com/v1/agents');
        if (!response.ok) {
            throw new Error('Failed to fetch agent data');
        }
        const data = await response.json();

        // Filter out unplayable characters
        agentsData = data.data.filter(agent => agent.isPlayableCharacter);

        showAgents(currentPage);
    } catch (error) {
        console.error('API fetch error:', error);
    }
}

// Function to show a specific set of agents
function showAgents(page = currentPage) {
    const start = page * agentsPerPage;
    const end = start + agentsPerPage;
    const playableAgents = agentsData.slice(start, end);

    // Clear existing agents
    agentContainer.innerHTML = '';

    // Populate agents
    playableAgents.forEach(agent => {
        const agentDiv = document.createElement('div');
        agentDiv.classList.add('agent');

        const agentImageElement = document.createElement('img');
        agentImageElement.src = agent.bustPortrait;
        agentImageElement.addEventListener('click', () => showAgentDetails(agent));
        agentDiv.appendChild(agentImageElement);

        if (agent.isPlayableCharacter) {
            const agentNameElement = document.createElement('div');
            agentNameElement.classList.add('agent-name');
            agentNameElement.textContent = agent.displayName; // Display agent names
            agentDiv.appendChild(agentNameElement);
        }

        agentContainer.appendChild(agentDiv);
    });

    updatePaginationButtons();
}

// Function to show the next set of agents
function showNextAgents() {
    currentPage = Math.min(currentPage + 1, Math.ceil(agentsData.length / agentsPerPage) - 1);
    showAgents();
}

// Function to show the previous set of agents
function showPrevAgents() {
    currentPage = Math.max(currentPage - 1, 0);
    showAgents();
}

// Function to show agent details
function showAgentDetails(agent) {
    const agentDetailsElement = document.getElementById('agentDetails');
    agentDetailsElement.style.display = 'block';

    // Fetch the background gradient colors for the agent
    const backgroundGradientColors = agent.backgroundGradientColors;

    // Define CSS variables for the gradient colors
    document.documentElement.style.setProperty('--gradient-color-1', `#${backgroundGradientColors[0]}`);
    document.documentElement.style.setProperty('--gradient-color-2', `#${backgroundGradientColors[1]}`);
    document.documentElement.style.setProperty('--gradient-color-3', `#${backgroundGradientColors[2]}`);
    document.documentElement.style.setProperty('--gradient-color-4', `#${backgroundGradientColors[3]}`);

    // Apply the background gradient to the agent details element
    agentDetailsElement.style.background = `linear-gradient(to bottom, ${backgroundGradientColors.join(', ')})`;

    // Get elements for the agent details view
    const agentImageElement = document.getElementById('agentImage');
    const agentNameElement = document.getElementById('agentName');
    const agentDescriptionElement = document.getElementById('agentDescription');
    const agentRoleElement = document.getElementById('agentRole');
    const roleDescriptionElement = document.getElementById('roleDescription');
    const skillsElement = document.getElementById('skills');

    // Update the elements with agent data
    agentImageElement.src = agent.displayIcon; // Use displayIcon
    agentNameElement.textContent = agent.displayName;
    agentDescriptionElement.textContent = agent.description;
    agentRoleElement.textContent = agent.role.displayName;
    roleDescriptionElement.textContent = agent.role.description;

    // Clear existing skills
    skillsElement.innerHTML = '';

    // Display agent skills
    const skillMappings = {
        "Ability1": "Ability 1",
        "Ability2": "Ability 2",
        "Ability3": "Ability 3",
        "Ultimate": "Ultimate",
        "Passive": "Passive",
        "Grenade": "Grenade"
    };

    agent.abilities.forEach(ability => {
        if (ability.displayName) {
            const skillElement = document.createElement('div');
            skillElement.classList.add('skill');
            skillElement.innerHTML = `
                <div class="skill-details">
                    <img src="${ability.displayIcon}" alt="${ability.displayName}">
                    <div class="skill-text">
                        <h4>${skillMappings[ability.slot]}: ${ability.displayName}</h4>
                        <p>${ability.description}</p>
                    </div>
                </div>
            `;
            skillsElement.appendChild(skillElement);
        }
    });
}

function closeAgentDetails() {
    const agentDetailsElement = document.getElementById('agentDetails');
    agentDetailsElement.style.display = 'none';

    // Clear the agent details view
    const skillsElement = document.getElementById('skills');
    skillsElement.innerHTML = '';
}

// Function to filter agents by name
function searchAgents() {
    const searchTerm = searchInput.value.toLowerCase();
    const filteredAgents = agentsData.filter(agent => agent.displayName.toLowerCase().includes(searchTerm));
    showAgentsList(filteredAgents);
}

// Function to filter agents by role
function filterAgentsByRole() {
    const selectedRole = roleFilter.value;
    filteredAgents = agentsData.filter(agent => agent.isPlayableCharacter && (selectedRole === 'all' || agent.role.displayName === selectedRole));
    currentPage = 0; // Reset the current page when applying a filter
    showFilteredAgents();
}

// Function to show the filtered agents with pagination
function showFilteredAgents() {
    const start = currentPage * agentsPerPage;
    const end = start + agentsPerPage;
    const agentsToDisplay = filteredAgents.slice(start, end);
    showAgentsInContainer(agentsToDisplay);

    // Disable previous/next buttons when there are no more agents to display
    prevButton.disabled = currentPage === 0;
    nextButton.disabled = end >= filteredAgents.length;
}

prevButton.addEventListener('click', showPrevAgents);
nextButton.addEventListener('click', showNextAgents);

// Function to show the next set of agents from the filtered array
function showNextAgents() {
    currentPage = Math.min(currentPage + 1, Math.floor(filteredAgents.length / agentsPerPage));
    showFilteredAgents();
}

// Function to show the previous set of agents from the filtered array
function showPrevAgents() {
    currentPage = Math.max(currentPage - 1, 0);
    showFilteredAgents();
}

// Function to show agents in the agent container
function showAgentsInContainer(agents) {
    agentContainer.innerHTML = '';

    agents.forEach(agent => {
        const agentDiv = document.createElement('div');
        agentDiv.classList.add('agent');

        const agentImageElement = document.createElement('img');
        agentImageElement.src = agent.bustPortrait;
        agentImageElement.addEventListener('click', () => showAgentDetails(agent));
        agentDiv.appendChild(agentImageElement);

        if (agent.isPlayableCharacter) {
            const agentNameElement = document.createElement('div');
            agentNameElement.classList.add('agent-name');
            agentNameElement.textContent = agent.displayName; // Display agent names
            agentDiv.appendChild(agentNameElement);
        }

        agentContainer.appendChild(agentDiv);
    });
}

// Function to filter agents by role
function filterAgents() {
    const selectedRole = roleFilter.value.toLowerCase();
    const filteredAgents = agentsData.filter(agent => selectedRole === 'all' || agent.role.displayName.toLowerCase() === selectedRole);

    // Calculate the current page based on the selected role
    currentPage = 0;

    showAgentsList(filteredAgents);
}

function showAgentsList(agents) {
    const start = currentPage * agentsPerPage;
    const end = start + agentsPerPage;
    const agentsToDisplay = agents.slice(start, end);

    agentContainer.innerHTML = '';
    agentsToDisplay.forEach(agent => {
        const agentDiv = document.createElement('div');
        agentDiv.classList.add('agent');

        const agentImageElement = document.createElement('img');
        agentImageElement.src = agent.bustPortrait;
        agentImageElement.addEventListener('click', () => showAgentDetails(agent));
        agentDiv.appendChild(agentImageElement);

        if (agent.isPlayableCharacter) {
            const agentNameElement = document.createElement('div');
            agentNameElement.classList.add('agent-name');
            agentNameElement.textContent = agent.displayName; // Display agent names
            agentDiv.appendChild(agentNameElement);
        }

        agentContainer.appendChild(agentDiv);
    });
}


// Initialize and fetch agent data from the API
fetchAgentsFromAPI();

// Add event listeners
searchInput.addEventListener('keyup', searchAgents);
roleFilter.addEventListener('change', filterAgents);
