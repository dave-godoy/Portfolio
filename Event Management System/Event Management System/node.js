const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

const db = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'event db'
})

app.use(express.json({}));
app.use(express.urlencoded({extended: true}));
app.use('/css',express.static(path.join(__dirname,'css')));
app.use('/images',express.static(path.join(__dirname,'images')));

var sessionStore = new MySQLStore({
    expiration: 10800000,
    createDatabaseTable: true,
    schema:{
        tableName: 'sessiontbl',
        columnNames:{
            session_id: 'sesssion_id',
            expires: 'expires',
            data: 'data'
        }
    }
},db);

app.use( session({
    key: 'keyin',
    secret: 'my secret',
    store: sessionStore,
    resave: false,
    saveUninitialized: true
}))

db.connect((err) =>{
    if (err) {
        console.log('Error Connecting to DB : ',err );
        return;
    }
    console.log('Connected To Database');
});



app.get('/', (req, res) => {
    if(!req.session.userInfo){
        res.sendFile(path.join(__dirname,'login.html'))
    }else{
        res.sendFile(path.join(__dirname,'homepage.html'))
    }

});

app.get('/signup', (req, res) => {
    res.sendFile(path.join(__dirname,'signup.html'))
});

app.post('/signup', (req, res) => {
    const {email,firstName,lastName,username,password} = req.body;
    const queryID = "SELECT * FROM users";
    db.query(queryID,(err,result,field) =>{
        if (err) {
            console.log("errur");
            res.send("error");
        }else {
            const ID = result.length;
            const insertUserQuery = 'INSERT INTO `users` (`ID`,`Email`, `FirstName`, `LastName`, `Username`, `Type`, `Password`) VALUES (?,?,?,?,?,?,?);';
            db.query(insertUserQuery, [ID,email,firstName,lastName,username,'STUDENT',password], (err) => {
                if (err) {
                    console.log('Error during signup:', err);
                    res.send('Error during signup');
                } else {
                    res.redirect('/login')
                }
            });
        }
    })
    
    
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname,'login.html'))
});

app.get('/homepage',(req,res) => {
        res.sendFile(path.join(__dirname,"/homepage.html"));
});

app.get('/form.html',(req,res)=>{

    res.sendFile(path.join(__dirname,`/form.html`))
})

app.post('/submitAttendance',(req,res)=> {
    const {EventName,subevents,userID} = req.body;
    var attendID;
    const query = `SELECT EventID from events where EventName = ?`;
    db.query(query,[EventName],(err,result,field)=>{
        if(err){
            console.log("no event found");
        }else{
            const id = result[0].EventID;
            subevents.forEach(element =>{
                const query2 = `SELECT SubeventID FROM subevents where SubeventName = ?`;
                db.query(query2,[element],(err,result1,field)=>{
                    if(err){
                        console.log(err);
                    }else{
                                const query4 = `INSERT INTO userattendance (AttendanceID, UserID, EventID, SubeventID, AttendanceDate) VALUES (?,?,?,?,?)`;
                                const currentDate = new Date();
                                const formattedDate = currentDate.toISOString().split('T')[0];
                                db.query(query4,[null,userID,id,result1[0].SubeventID,formattedDate],(err,result5,field)=>{
                                    if(err){
                                        console.log(err);
                                    }else{
                                        console.log('Insert of data success');
                                        attendID++;
                                    }
                                })
                            }
                        })
                    })
                    res.send("attendance is taken successfully you may now check your profile for the newly added attendance!!!!")
                }
            })
        })

app.get('/getData',(req,res)=>{
    const query = `SELECT * FROM events`;
    db.query(query, (err,result,field) => {
        if (err) {
            console.error('Error retrieving data:', error);
            res.status(500).send('Internal Server Error');
            return;
        }
        const responseData = result.map(result => ({
            ID : result.EventID,
            EventName: result.EventName,
            Date: String(result.EventDate).substring(0,15),
            Location: result.EventLocation,
            Description: result.Description,
            Organizer: result.Organizer,
            Img: `data:image/jpg;base64,${Buffer.from(result.EventLogo).toString('base64')}`,
          }));
          console.log(responseData);
          // Send the data as a JSON response
          res.json(responseData);
        });
});

app.get('/getSubEventAndEvent',(req,res)=>{
    const {ID,subID} = req.query;
    console.log(ID,subID);
    const query1 = "select * from events where EventID = ?";
    const query2 = "select * from subevents where SubeventID = ?";
    db.query(query1,[ID],(err,result1) =>{
        db.query(query2,[subID],(err,result2)=>{
            const dataResponse = {
                json1 : result1,
                json2 : result2
            }
            console.log(dataResponse);
            res.json(dataResponse);
        })
    })
})

app.post('/login', (req, res) => {
    const { email, password } = req.body;

    const getUserQuery = 'SELECT * FROM users WHERE Email = ? AND Password = ?';
    db.query(getUserQuery, [email, password], (err, results) => {
        if (err) {
            console.log('Error during login:', err);
            res.send('Error during login');
        } else {
            if (results.length === 1) {
                req.session.destroy;
                console.log(results[0].FirstName)
                req.session.userInfo = results[0].FirstName;
                req.session.userID = results[0].ID;
                res.redirect("/homepage")
            } else {
                res.status = 400;
                res.send('Invalid username or password. <a href="/login">Try again</a>');
            }
        }
    });
});

app.get('/getUserData',(req,res) => {
    const id = req.session.userID;
    console.log(id);
    const query1 = 'SELECT * FROM `users` where ID = ?'
    db.query(query1,[id],(err1,result1,field1) =>{
        const query2 = 'SELECT * FROM `userattendance` where UserID = ?';
        db.query(query2,[id],(err2,result2,field2)=>{
            const jsonToSend = {
                json1 : result1,
                json2 : result2
            }
            console.log(jsonToSend);
            res.json(jsonToSend);
        }) 
    })
})

app.get('/userData',(req,res)=>{   
    const data = {name : `${req.session.userInfo}`};
    const guest = {name : 'GUEST'}
    if(req.session.userInfo){
        console.log(data);
        res.json(data);
    }else{
        console.log(guest);
        res.json(guest);
    }
})

app.get('/getRegistrationData',(req,res)=>{
    const query = "select * from registration where UserID = ?";
    db.query(query,[req.session.userID],(err,result)=>{
        res.json(result);
    })
})

app.get('/profile',(req,res)=>{
    if(req.session.userInfo){
    res.sendFile(path.join(__dirname,'profile.html'))
    }else{
    res.send("You are not Logged In you are using as guest!")
    }
})

app.get(`/event_detail`,(req,res)=>{
    res.sendFile(path.join(__dirname,'event_detail.html'));
    
})
app.get('/getEvent',(req,res)=>{
    const ID = req.query.ID;
    const query = `SELECT * FROM events where EventID = ${ID}`
    const query2 = `SELECT * FROM subevents where EventID = ${ID}`
    db.query(query,[ID],(err,result1,field)=>{
        if(err) {
            console.log("error in querying1")
            res.send("error in query1")
        }else {
            db.query(query2,[ID],(err,result2,field)=>{
                var Ongoing = 0; 
                var Done = 0;
                if (new Date(result1[0].EventDate) < new Date() && new Date(result1[0].EventDateEnd) > new Date()){
                    Ongoing = 1;
                }
                if (new Date(result1[0].EventDateEnd) < new Date()){
                    Done = 1;
                }
                console.log(Ongoing);
                const json1 = result1.map(result1 => ({
                    userID : req.session.userID,
                    ID : result1.EventID,
                    EventName: result1.EventName,
                    Date: String(result1.EventDate).substring(0,15),
                    DateEnd: String(result1.EventDateEnd).substring(0,15),
                    Location: result1.EventLocation,
                    Description: result1.Description,
                    Organizer: result1.Organizer,
                    Img: `data:image/jpg;base64,${Buffer.from(result1.EventLogo).toString('base64')}`,
                    OnGoing: Ongoing,
                    done : Done
                }))
                const responseData = {
                    json1 : json1,
                    json2 : result2
                }
                console.log(responseData);
                res.json(responseData);
            })
        }
    })
})

app.get('/registration',(req,res)=>{
    const eventID = req.query.eventID;
    console.log(eventID);
    var registeredC = 0;
    const query = "Select * from registration where EventID = ? and UserID = ?"
    db.query(query,[eventID,req.session.userID],(err,result,field)=>{
        if (result && result.length > 0){
            registeredC = 1 ;
        }
        const jsonToSend = {
            registered : registeredC
        }
        res.json(jsonToSend);
    })
})

app.post('/register',(req,res)=>{
    const eventID = req.query.eventID;
    const query = "INSERT INTO `registration` (`RegistrationID`, `EventID`, `UserID`) VALUES (?,?,?);";
    db.query(query,[null,eventID,req.session.userID]);
    res.send("Registered Successfully")
})

app.get('/about',(req,res)=>{
    res.sendFile(path.join(__dirname,'about.html'));
})

app.get('/scanner',(req,res)=>{
    res.sendFile(path.join(__dirname,'scanner.html'));
})


app.use('/logout', function(req,res){
    req.session.destroy(function(err){
        if(!err){
            res.sendFile(path.join(__dirname,'login.html'));
        }
    })
})

const PORT = 3000;
app.listen(PORT,() => {
    console.log(`Server is running on port ${PORT}`);
});