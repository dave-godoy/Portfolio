document.addEventListener("DOMContentLoaded", function () {
    const videoLinks = document.querySelectorAll('.image-link');

    videoLinks.forEach(link => {
        const video = link.querySelector('.video');

        link.addEventListener('mouseover', function () {
            video.play();
        });

        link.addEventListener('mouseout', function () {
            video.pause();
        });
    });
});

