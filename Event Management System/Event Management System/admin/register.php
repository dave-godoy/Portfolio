<?php
include 'db_connect.php';
include 'navbar.php';

// Fetch event details and existing subevents
if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['eventId'])) {
    $eventId = $_GET['eventId'];

    $eventQuery = "SELECT * FROM events WHERE EventID = $eventId";
    $eventResult = $conn->query($eventQuery);

    if ($eventResult->num_rows > 0) {
        $eventRow = $eventResult->fetch_assoc();
        $eventName = $eventRow['EventName'];

        $subeventsQuery = "SELECT SubeventID, SubeventName FROM subevents WHERE EventID = $eventId";
        $subeventsResult = $conn->query($subeventsQuery);

        $existingSubevents = [];
        if ($subeventsResult->num_rows > 0) {
            while ($subeventRow = $subeventsResult->fetch_assoc()) {
                $existingSubevents[$subeventRow['SubeventID']] = $subeventRow['SubeventName'];
            }
        }
    } else {
        echo "Event not found.";
        exit;
    }
} else {
    echo "Invalid request.";
    exit;
}

// Handle manual registration form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['manualRegistration'])) {
    $userId = $_POST['userId'];
    $selectedSubevents = isset($_POST['subevents']) ? $_POST['subevents'] : [];

    if ($userId && $selectedSubevents) {
        $attendanceDate = date('Y-m-d H:i:s');

        foreach ($selectedSubevents as $subeventId) {
            $insertQuery = "INSERT INTO userattendance (UserID, EventID, SubeventID, AttendanceDate) 
                            VALUES ($userId, $eventId, $subeventId, '$attendanceDate')";
            $result = $conn->query($insertQuery);

            if (!$result) {
                echo "Failed to register. Error: " . $conn->error;
                exit;
            }
        }

        echo "Successfully registered manually for UserID: $userId.";
    } else {
        echo "Invalid input for manual registration.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Register for Event</title>
    <link rel="stylesheet" href="register.css">
</head>

<body>

    <div class="container">
        <div class="register-container">
            <h2>Register for Event: <?php echo $eventName; ?></h2>

            <form method="post" action="process_registration.php">
                <input type="hidden" name="eventId" value="<?= $eventId ?>">

                <h3>Manual Registration</h3>
                <label for="userId">Student ID:</label>
                <input type="text" name="userId" required>

                <br><br>
                <label>Subevents:</label><br>
                <?php foreach ($existingSubevents as $subeventID => $subeventName) : ?>
                    <label>
                        <input type="checkbox" name="subevents[]" value="<?= $subeventID ?>"> <?= $subeventName ?>
                    </label><br>
                <?php endforeach; ?>

                <br><br>
                <button type="submit" name="manualRegistration">Register Manually</button>
            </form>
        </div>
    </div>
</body>

</html>
