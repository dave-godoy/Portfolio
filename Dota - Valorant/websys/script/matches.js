// Function to fetch and display data from the OpenDota API
function fetchMatches() {
    const matchesContainer = document.getElementById('matches-container');

    fetch('https://api.opendota.com/api/proMatches')
        .then(response => response.json())
        .then(data => {
            data.forEach(match => {
                const matchDiv = document.createElement('div');
                matchDiv.classList.add('match-container');

                if (match.radiant_win) {
                    matchDiv.classList.add('radiant-win');
                    var direNameClass = 'dire-team-black';
                    var radiantNameClass = 'radiant-team-blue';
                } else {
                    matchDiv.classList.add('dire-win');
                    var direNameClass = 'dire-team-red';
                    var radiantNameClass = 'radiant-team-black';
                }

                const direTeam = `<span class="${direNameClass}">${match.dire_name} (${match.dire_score})</span>`;
                const radiantTeam = `<span class="${radiantNameClass}">${match.radiant_name} (${match.radiant_score})</span>`;

                matchDiv.innerHTML = `
                    <div class="match-content">
                        <p>League: ${match.league_name}</p>
                        <p>${direTeam} vs ${radiantTeam}</p>
                        <p>Duration: ${formatMatchDuration(match.duration)}</p>
                    </div>
                    <div class="match-right">
                        <a href="/websys/html/match_details.html?matchID=${match.match_id}" target="_blank" class="view-match-link">View Match History</a>
                    </div>
                `;

                matchesContainer.appendChild(matchDiv);
            });
        });
}

function formatMatchDuration(duration) {
    const minute = Math.floor(duration / 60);
    const second = duration % 60;
    return `${minute}:${second < 10 ? '0' : ''}${second}`;
}

// Call fetchMatches initially
fetchMatches();

// Set up a refresh interval
const refreshInterval = 10000;
setInterval(fetchMatches, refreshInterval);
