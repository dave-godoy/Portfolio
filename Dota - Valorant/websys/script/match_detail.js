// Function to fetch and display data from the OpenDota API
function fetchMatches() {
    const matchesContainer = document.getElementById('matches-container');

    fetch('https://api.opendota.com/api/proMatches')
        .then(response => response.json())
        .then(data => {
            data.forEach(match => {
                const matchDiv = document.createElement('div');
                matchDiv.classList.add('match-container');

                if (match.radiant_win) {
                    matchDiv.classList.add('radiant-win');
                    var direNameClass = 'dire-team-black';
                    var radiantNameClass = 'radiant-team-blue';
                } else {
                    matchDiv.classList.add('dire-win');
                    var direNameClass = 'dire-team-red';
                    var radiantNameClass = 'radiant-team-black';
                }

                const direTeam = `<span class="${direNameClass}">${match.dire_name} (${match.dire_score})</span>`;
                const radiantTeam = `<span class="${radiantNameClass}">${match.radiant_name} (${match.radiant_score})</span>`;

                matchDiv.innerHTML = `
                    <div class="match-content">
                        <p>League: ${match.league_name}</p>
                        <p>${direTeam} vs ${radiantTeam}</p>
                        <p>Duration: ${formatMatchDuration(match.duration)}</p>
                    </div>
                    <div class="match-right">
                        <a href="/websys/html/match_details.html?matchID=${match.match_id}" target="_blank" class="view-match-link">View Match History</a>
                    </div>
                `;

                matchesContainer.appendChild(matchDiv);
            });
        });
}

function formatMatchDuration(duration) {
    const minute = Math.floor(duration / 60);
    const second = duration % 60;
    return `${minute}:${second < 10 ? '0' : ''}${second}`;
}

// Call fetchMatches initially
fetchMatches();

// Set up a refresh interval
const refreshInterval = 10000;
setInterval(fetchMatches, refreshInterval);

// Function to get query parameters from the URL
function getQueryParameter(name) {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(name);
}

// Get the matchID parameter
const matchID = getQueryParameter("matchID");

function loadDetails() {
    fetch(`https://api.opendota.com/api/matches/${matchID}`)
        .then(response => response.json())
        .then(data => {
            // Update match result details
            const matchWinner = document.getElementById('matchWinner');
            const matchLoser = document.getElementById('matchLoser');
            const radiantScore = document.getElementById('radiantScore');
            const direScore = document.getElementById('direScore');
            const matchDuration = document.getElementById('matchDuration');
            const radiantLogo = document.getElementById('radiantLogo');
            const direLogo = document.getElementById('direLogo');
            
            // Update team names
            const radiantName = data.radiant_team.name;
            const direName = data.dire_team.name;

            if (data.radiant_win) {
                matchWinner.textContent = radiantName;
                matchLoser.textContent = direName;
            } else {
                matchWinner.textContent = direName;
                matchLoser.textContent = radiantName;
            }

            radiantScore.textContent = data.radiant_score;
            direScore.textContent = data.dire_score;
            matchDuration.textContent = `Match Duration: ${formatMatchDuration(data.duration)}`;
            radiantLogo.src = data.radiant_team.logo_url;
            direLogo.src = data.dire_team.logo_url;

            // Update team names in the h3 elements
            const radiantTeamName = document.getElementById('radiantTeamName');
            const direTeamName = document.getElementById('direTeamName');
            radiantTeamName.textContent = radiantName;
            direTeamName.textContent = direName;

            // Update team logos and hero containers here
            const radiantHeroesContainer = document.getElementById('radiantHeroes');
            const direHeroesContainer = document.getElementById('direHeroes');
            data.picks_bans.forEach(element => {
                if (element.is_pick === true) {
                    findHeroNameById(element.hero_id)
                        .then(heroName => {
                            if (heroName !== null) {
                                const divRad = document.createElement('div');
                                divRad.className = "hero-card";
                                const img = document.createElement('img');
                                const textElement = document.createElement('h4');
                                img.src = `../images/${heroName.replace(/ /g, '_')}.png`;
                                img.className = "hero-image"; // Apply the hero-image class
                                textElement.textContent = heroName;
                                textElement.className = "hero-name".replace(/_/g, ' '); // Apply the hero-name class
                                divRad.appendChild(img);
                                divRad.appendChild(textElement);
                                if (element.team === 0) {
                                    radiantHeroesContainer.appendChild(divRad);
                                } else {
                                    direHeroesContainer.appendChild(divRad);
                                }
                            } else {
                                console.log("Error finding hero name");
                            }
                        })
                        .catch(error => {
                            console.error("An error occurred:", error);
                        });
                }
            });
        });
}
loadDetails();

function findHeroNameById(heroId) {
    return fetch("https://api.opendota.com/api/heroes")
        .then(response => response.json())
        .then(data => {
            const hero = data.find(hero => hero.id === heroId);
            if (hero) {
                // Manually capitalize the first letter of the hero name
                const heroName = hero.name.replace('npc_dota_hero_', '').replace(/ /g, '_');
                return heroName.charAt(0).toUpperCase() + heroName.slice(1);
            } else {
                return null; // Return null if no matching hero is found
            }
        });
}

