<?php include 'navbar.php'; ?>
<?php include 'db_connect.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="get_users.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.view-history').click(function () {
                var userId = $(this).data('id');
                window.open('view_user_history.php?userId=' + userId, '_blank');
            });
        });
    </script>
</head>

<body>
    <div class="container-fluid admin">
        <div class="col-md-12 alert alert-primary">List of Users</div>
        <br>
        <br>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered" id='table'>
                        <colgroup>
                            <col width="15%">
                            <col width="35%">
                            <col width="25%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>History</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT ID, CONCAT(FirstName, ' ', LastName) AS UserName, Email FROM users";
                            $result = $conn->query($query);

                            if ($result !== false) {
                                if ($result->num_rows > 0) {
                                    $i = 1;
                                    while ($row = $result->fetch_assoc()) {
                                        echo "<tr>";
                                        echo "<td>{$i}</td>";
                                        echo "<td>{$row['UserName']}</td>";
                                        echo "<td>{$row['Email']}</td>";
                                        echo "<td><button class='btn btn-primary btn-sm view-history' data-id='{$row['ID']}'>History</button></td>";
                                        echo "</tr>";
                                        $i++;
                                    }
                                } else {
                                    echo "<tr><td colspan='4'>No users found.</td></tr>";
                                }
                            } else {
                                echo "<tr><td colspan='4'>Error executing the query: " . $conn->error . "</td></tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>


</html>
