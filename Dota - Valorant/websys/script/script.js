function fetchHeroData() {
    return fetch("https://api.opendota.com/api/heroes")
        .then(response => response.json())
        .then(data => {
            return data.map(hero => ({
                ...hero,
                image_path: `../images/${hero.name.replace('npc_dota_hero_', '').replace(/ /g, '_')}.png`

            }));
        });
}


function filterHeroes() {
    const roleFilter = document.getElementById("roleFilter").value;
    const attrFilter = document.getElementById("attrFilter").value;
    const heroesContainer = document.getElementById("heroesContainer");

    heroesContainer.innerHTML = "";

    fetchHeroData()
        .then(heroData => {
            heroData.forEach(hero => {
                if (
                    (roleFilter === "all" || hero.roles.includes(roleFilter)) &&
                    (attrFilter === "all" || hero.primary_attr === attrFilter)
                ) {
                    const heroCard = document.createElement("div");
                    heroCard.classList.add("hero-card");

                    const heroImage = document.createElement("img");
                    heroImage.classList.add("hero-image");
                    heroImage.src = hero.image_path;
                    heroCard.appendChild(heroImage);

                    const heroName = document.createElement("div");
                    heroName.classList.add("hero-name");
                    heroName.textContent = hero.localized_name;
                    heroCard.appendChild(heroName);

                    heroesContainer.appendChild(heroCard);
                }
            });
        });
}

function displayHeroes() {
    const heroesContainer = document.getElementById("heroesContainer");

    fetchHeroData().then(heroData => {
        heroData.forEach(hero => {
            const heroCard = document.createElement("div");
            heroCard.classList.add("hero-card");

            const heroImage = document.createElement("img");
            heroImage.classList.add("hero-image");
            heroImage.src = hero.image_path;
            heroCard.appendChild(heroImage);

            const heroName = document.createElement("div");
            heroName.classList.add("hero-name");
            heroName.textContent = hero.localized_name;
            heroCard.appendChild(heroName);

            heroesContainer.appendChild(heroCard);
        });
    });
}

// Initial call to populate heroes without filtering
displayHeroes();

// Event listeners for role and attribute filters
document.getElementById("roleFilter").addEventListener("change", filterHeroes);
document.getElementById("attrFilter").addEventListener("change", filterHeroes);
