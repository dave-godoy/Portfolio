<?php
include 'navbar.php';
include 'db_connect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="styles.css"> 
    <style>
        .title {
            text-align: center;
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .center-text {
            text-align: center;
        }
    </style>
</head>

<body>
    <?php
    // Check if the user ID is provided in the URL
    if (isset($_GET['userId'])) {
        $userId = $_GET['userId'];

        // Fetch user information
        $userQuery = "SELECT ID, CONCAT(FirstName, ' ', LastName) AS UserName, Email FROM users WHERE ID = $userId";
        $userResult = $conn->query($userQuery);

        // Check if the query for user information was successful
        if ($userResult !== false && $userResult->num_rows > 0) {
            $user = $userResult->fetch_assoc();
            $userName = $user['UserName'];
            $email = $user['Email'];
            $userId = $user['ID'];

            // Fetch user's attendance history
            $attendanceQuery = "SELECT ua.AttendanceID, e.EventName, s.SubeventName, ua.AttendanceDate
                                FROM userattendance ua
                                JOIN events e ON ua.EventID = e.EventID
                                JOIN subevents s ON ua.SubeventID = s.SubeventID
                                WHERE ua.UserID = $userId
                                ORDER BY ua.AttendanceDate DESC";
            $attendanceResult = $conn->query($attendanceQuery);

            // Check if the query for user attendance history was successful
            if ($attendanceResult !== false && $attendanceResult->num_rows > 0) {
                echo "<div class='container mt-4'>";
                echo "<h2 class='title'>Attendance History</h2>";
                echo "<p>Email: $email</p>";
                echo "<p>User ID: $userId</p>";

                echo "<h3 class='center-text'>User History: $userName</h3>";
                echo "<table class='table table-bordered'>";
                echo "<colgroup>";
                echo "<col width='40%'>"; 
                echo "<col width='40%'>";
                echo "<col width='20%'>";
                echo "</colgroup>";
                echo "<thead><tr><th>Event</th><th>Subevent</th><th>Attendance Date</th></tr></thead>";
                echo "<tbody>";

                while ($row = $attendanceResult->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td class='center-text'>{$row['EventName']}</td>";
                    echo "<td class='center-text'>{$row['SubeventName']}</td>";
                    echo "<td class='center-text'>{$row['AttendanceDate']}</td>";
                    echo "</tr>";
                }

                echo "</tbody></table>";
                echo "</div>";
            } else {
                echo "<div class='container mt-4'><p>No attendance history found for the user.</p></div>";
            }
        } else {
            echo "<div class='container mt-4'><p>User not found.</p></div>";
        }
    } else {
        echo "<div class='container mt-4'><p>User ID not provided in the URL.</p></div>";
    }
    ?>
</body>


</html>
