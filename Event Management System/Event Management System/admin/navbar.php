<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="sidenav">
        <img src="/img-01.png" alt="Navbar Image" class="navbar-image">
        <a href="dashboard.php">Dashboard</a>
        <a href="get_users.php">Users</a>
        <a href="get_events.php">Current Events</a>
        <a href="add_event.php" target="_blank">Add New Event</a>
        <a href="qr.php">Scanner</a>
    </div>
</body>
</html>
