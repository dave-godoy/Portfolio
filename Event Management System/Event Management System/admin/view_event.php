<?php
include 'db_connect.php';

if (isset($_GET['eventId'])) {
    $eventId = $_GET['eventId'];

    // Fetch event details
    $eventQuery = "SELECT * FROM events WHERE EventID = $eventId";
    $eventResult = $conn->query($eventQuery);

    if ($eventResult->num_rows > 0) {
        $eventRow = $eventResult->fetch_assoc();

        // Fetch associated subevents
        $subeventsQuery = "SELECT SubeventName FROM subevents WHERE EventID = $eventId";
        $subeventsResult = $conn->query($subeventsQuery);

        ?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <link rel="stylesheet" href="styles.css">
            <style>
                body {
                    font-family: Arial, sans-serif;
                    background-color: #f4f4f4;
                    margin: 0;
                    padding: 0;
                }

                .container {
                    max-width: 800px;
                    margin: 20px auto;
                    background-color: #fff;
                    padding: 20px;
                    border-radius: 8px;
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                }

                label {
                    font-weight: bold;
                }

                img {
                    max-width: 100%;
                    height: auto;
                    border-radius: 8px;
                    margin-top: 10px;
                }

                span {
                    display: block;
                    margin-bottom: 10px;
                }

                #event-logo {
                    width: auto; 
                    height: auto;
                    margin-top: 10px;
                }
            </style>
        </head>

        <body>
            <div class="container">
                <h2>Event Details</h2>
                <div>
                    <label>Event Logo:</label>
                    <img id="event-logo" src='data:image/png;base64,<?= base64_encode($eventRow['EventLogo']) ?>' alt='Event Logo'>
                </div>
                <div>
                    <label>Event Name:</label>
                    <span><?= $eventRow['EventName'] ?></span>
                </div>
                <div>
                    <label>Event Date:</label>
                    <span><?= $eventRow['EventDate'] ?></span>
                </div>
                <div>
                    <label>Event Location:</label>
                    <span><?= $eventRow['EventLocation'] ?></span>
                </div>
                <div>
                    <label>Organizer:</label>
                    <span><?= $eventRow['Organizer'] ?></span>
                </div>
                <div>
                    <label>Description:</label>
                    <span><?= $eventRow['Description'] ?></span>
                </div>

                <!-- Display subevents -->
                <div>
                    <label>Subevents:</label>
                    <?php
                    if ($subeventsResult->num_rows > 0) {
                        while ($subeventRow = $subeventsResult->fetch_assoc()) {
                            echo "<span>{$subeventRow['SubeventName']}</span>";
                        }
                    } else {
                        echo "<span>No subevents available for this event.</span>";
                    }
                    ?>
                </div>
            </div>
        </body>

        </html>
        <?php
    } else {
        echo "<p>Event not found.</p>";
    }
} else {
    echo "<p>Invalid request.</p>";
}
?>
