<?php
include 'db_connect.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['eventId'])) {
    $eventId = $_POST['eventId'];
    $eventName = $_POST['eventName'];
    $eventDate = $_POST['eventDate'];
    $eventDateEnd = $_POST['eventDateEnd'];
    $eventLocation = $_POST['eventLocation'];
    $eventOrganizer = $_POST['eventOrganizer'];
    $eventDescription = $_POST['eventDescription'];

    // Check if a new logo is uploaded
    if ($_FILES['newLogo']['size'] > 0) {
        // Process the new logo
        $newLogo = addslashes(file_get_contents($_FILES['newLogo']['tmp_name']));
        $query = "UPDATE events SET EventName='$eventName', EventDate='$eventDate', EventDateEnd='$eventDateEnd', EventLocation='$eventLocation', Organizer='$eventOrganizer', EventLogo='$newLogo', Description='$eventDescription' WHERE EventID=$eventId";
    } else {
        // Keep the existing logo
        $query = "UPDATE events SET EventName='$eventName', EventDate='$eventDate', EventDateEnd='$eventDateEnd', EventLocation='$eventLocation', Organizer='$eventOrganizer', Description='$eventDescription' WHERE EventID=$eventId";
    }

    if ($conn->query($query) === TRUE) {
        // Process existing subevents
        $editSubevents = isset($_POST['editSubevents']) ? $_POST['editSubevents'] : '';
        if (is_array($editSubevents)) {
            foreach ($editSubevents as $subeventId => $subeventName) {
                // Trim and sanitize input
                $subeventId = trim($subeventId);
                $subeventName = trim($subeventName);

                // Check if both subeventId and subeventName are set
                if (!empty($subeventId) && !empty($subeventName)) {
                    // Update subevent name while retaining subevent and event IDs
                    $updateSubeventQuery = "UPDATE subevents SET SubeventName='$subeventName' WHERE SubeventID='$subeventId'";
                    $conn->query($updateSubeventQuery);
                } else {
                    echo "Invalid format for existing subevent: SubeventID=$subeventId, SubeventName=$subeventName";
                }
            }
        }

        // Process new subevents
        $newSubevents = isset($_POST['newSubevents']) ? $_POST['newSubevents'] : [];
        $newSubeventsArray = array_filter(array_map('trim', $newSubevents));
        foreach ($newSubeventsArray as $index => $newSubevent) {
            $subeventId = $eventId . str_pad($index + 1, 2, '0', STR_PAD_LEFT);
            $insertSubeventQuery = "INSERT INTO subevents (SubeventID, SubeventName, EventID) VALUES ('$subeventId', '$newSubevent', $eventId)";
            $conn->query($insertSubeventQuery);
        }

        echo "Event and subevents updated successfully!";
        header('Location: get_events.php'); // Redirect to get_events.php
        exit;
    } else {
        echo "Error updating event: " . $conn->error;
    }
} else {
    echo "Invalid request.";
}

$conn->close();
?>
