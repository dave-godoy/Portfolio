<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pie Charts</title>
    <link rel="stylesheet" href="dashboard.css"> 

</head>

<body>
    <?php
 
    require_once ('../jpgraph-4.4.2/src/jpgraph.php');
    require_once ('../jpgraph-4.4.2/src/jpgraph_pie.php');

    include 'db_connect.php';
    include 'navbar.php';


    function getSubeventNameById($conn, $subeventId) {
        $query = "SELECT SubeventName FROM subevents WHERE SubeventID = $subeventId";
        $result = $conn->query($query);

        if ($result && $result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row['SubeventName'];
        } else {
            return 'Subevent Name Not Found';
        }
    }

    // Fetch data from the database
    $query = "SELECT EventID, EventName FROM events";
    $result = $conn->query($query);

    while ($eventRow = $result->fetch_assoc()) {
        $eventId = $eventRow['EventID'];
        $eventName = $eventRow['EventName'];

        // Fetch data for the specific event from the userattendance table
        $queryEvent = "SELECT SubeventID, COUNT(AttendanceID) AS AttendeeCount
                  FROM userattendance
                  WHERE EventID = $eventId
                  GROUP BY SubeventID";
        $resultEvent = $conn->query($queryEvent);

        // Prepare data arrays for the pie chart
        $data = array();
        $legends = array();

        while ($row = $resultEvent->fetch_assoc()) {
            $attendeeCount = $row['AttendeeCount'];
            if ($attendeeCount > 0) {
                $data[] = $attendeeCount;
                $legends[] = getSubeventNameById($conn, $row['SubeventID']);
            }
        }

        // Check if there is data to create the pie chart
        if (!empty($data)) {
            // Create a pie chart for each event
            $graph = new PieGraph(600, 400);
            $graph->SetShadow();

            // Create a pie plot
            $pieplot = new PiePlot($data);

            // Set slice labels (legends)
            $pieplot->SetLegends($legends);

            // Add the pie plot to the graph
            $graph->Add($pieplot);

            // Set title
            $graph->title->Set("Attendees of - $eventName");

            // Output the image to a file
            $filename = "charts/pie_chart_event_$eventName.png";
            $graph->Stroke($filename);

            // Display HTML only if there is data
            echo "<div class='chart-container'>";
            echo "<h2>$eventName</h2>";
            echo "<img src='$filename' alt='Pie Chart - $eventName'>";
            echo "</div>";
        }
    }
    ?>
</body>

</html>
