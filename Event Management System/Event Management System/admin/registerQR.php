<?php
include 'db_connect.php';

// Get data from the POST request
$userID = $_POST['userID'];
$eventID = $_POST['eventID'];
$subEventID = $_POST['subEventID'];

// Check if the user has already taken attendance for the same event and subevent
$stmtCheck = $conn->prepare("SELECT * FROM userattendance WHERE UserID = ? AND EventID = ? AND SubeventID = ?");
$stmtCheck->bind_param("iii", $userID, $eventID, $subEventID);
$stmtCheck->execute();
$stmtCheck->store_result();

if ($stmtCheck->num_rows > 0) {
    // User has already taken attendance for this event and subevent
    echo "Attendance for this student has already been taken for this event and subevent.";
} else {
    // User hasn't taken attendance, proceed with insertion
    $attendanceDate = date("Y-m-d");

    $stmtInsert = $conn->prepare("INSERT INTO userattendance (UserID, EventID, SubeventID, AttendanceDate) VALUES (?, ?, ?, ?)");
    $stmtInsert->bind_param("iiis", $userID, $eventID, $subEventID, $attendanceDate);
    $stmtInsert->execute();

    if ($stmtInsert->affected_rows > 0) {
        echo "Data inserted successfully.";
    } else {
        echo "Error inserting data: " . $stmtInsert->error;
    }

    $stmtInsert->close();
}

$stmtCheck->close();
$conn->close();
?>
