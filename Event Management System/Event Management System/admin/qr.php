<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="scanner.css">
    <title>QR Code Scanner / Reader</title>
</head>
<body>
    <div class="container">
        <h1>Scan QR Codes</h1>
        <div class="section">
            <div id="my-qr-reader"></div>
        </div>
    </div>
    <script src="https://unpkg.com/html5-qrcode"></script>
    <script>
        function domReady(fn) {
            if (
                document.readyState === "complete" ||
                document.readyState === "interactive"
            ) {
                setTimeout(fn, 1000);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        domReady(function () {
            function onScanSuccess(decodeText, decodeResult) {
                // Parse the JSON data from the scanned QR code
                const jsonData = JSON.parse(decodeText);

                // Extract data from JSON
                const userID = jsonData.userID;
                const eventID = jsonData.eventID;
                const subEventID = jsonData.subEventID;

                // Call a function to send data to the server
                sendDataToServer(userID, eventID, subEventID);
            }

            let htmlscanner = new Html5QrcodeScanner(
                "my-qr-reader",
                { fps: 10, qrbos: 250 }
            );

            htmlscanner.render(onScanSuccess);

            // Function to send data to the server using AJAX
            function sendDataToServer(userID, eventID, subEventID) {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "registerQR.php", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            const data = `userID=${userID}&eventID=${eventID}&subEventID=${subEventID}`;
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        // Check the response from the server
                        if (xhr.responseText === "Attendance already taken.") {
                            // Display a prompt indicating that attendance has already been taken
                            alert("Attendance for this student has already been taken for this event and subevent.");
                        } else if (xhr.responseText === "Data inserted successfully.") {
                            // Display a success message or perform other actions
                            alert("Data inserted successfully!");
                        } else {
                            // Display an error message
                            alert(xhr.responseText);
                        }
                    } else {
                        // Display an error message
                        alert(xhr.status);
                    }
                }
            };

            xhr.send(data);
            }
        });
    </script>
</body>
</html>
