<?php
include 'navbar.php';
include 'db_connect.php';

function isEventInCategory($eventDate, $category, $currentDate) {
    switch ($category) {
        case 'all':
            return true;
        case 'done':
            return $eventDate < $currentDate;
        case 'this-week':
            $startOfWeek = date('Y-m-d', strtotime('last Sunday', strtotime($currentDate)));
            $endOfWeek = date('Y-m-d', strtotime('next Saturday', strtotime($currentDate)));
            return ($eventDate >= $startOfWeek && $eventDate <= $endOfWeek);
        case 'future':
            $startOfNextWeek = date('Y-m-d', strtotime('next Sunday', strtotime($currentDate)));
            return $eventDate > $startOfNextWeek;
        default:
            return false;
    }
}

$currentDate = date("Y-m-d");
$filterCategory = isset($_GET['filter']) ? $_GET['filter'] : 'all';

$query = "SELECT EventID, EventName, EventLogo, EventDate FROM events";
$result = $conn->query($query);

if ($result->num_rows > 0) {
    echo "<!DOCTYPE html>
    <html lang='en'>
    
    <head>
        <title>List of Events</title>
        <link rel='stylesheet' href='get_events.css'>
    </head>
    
    <body>
        <div class='container'>
            <h2>Event List</h2>
    
            <!-- Filter buttons -->
            <div class='filter-buttons'>
                <button class='btn btn-primary' id='all-events'>All</button>
                <button class='btn btn-primary' id='done-events'>Done</button>
                <button class='btn btn-primary' id='this-week-events'>This Week</button>
                <button class='btn btn-primary' id='future-events'>Future Events</button>
            </div>
    
            <div class='event-container'>";
    
    while ($row = $result->fetch_assoc()) {
        $eventDate = $row['EventDate'];
    
        if (isEventInCategory($eventDate, $filterCategory, $currentDate)) {
            echo "<div class='event-item'>";
            echo "<h3 class='event-name' data-id='{$row['EventID']}'>{$row['EventName']}</h3>";
            echo "<div class='button-container'>";
            echo "<button class='btn btn-primary btn-sm view-event' data-id='{$row['EventID']}'>View Event</button>";
            echo "<button class='btn btn-success btn-sm edit-event' data-id='{$row['EventID']}'>Edit Event</button>";
            echo "<button class='btn btn-success btn-sm register-event' data-id='{$row['EventID']}'>Attendance</button>";
            
            // Add the Registrants button
            echo "<button class='btn btn-info btn-sm registrants-event' data-id='{$row['EventID']}'>Registrants</button>";
    
            echo "</div>";
            echo "<img src='data:image/jpeg;base64," . base64_encode($row['EventLogo']) . "' alt='Event Image'>";
            echo "</div>";
        }
    }

    echo "</div></div>
        <script src='https://code.jquery.com/jquery-3.6.4.min.js'></script>
    
        <script>
            $(document).ready(function () {
                // Event handler for filter buttons
                $('#all-events').click(function () {
                    window.location.href = 'get_events.php?filter=all';
                });
    
                $('#done-events').click(function () {
                    window.location.href = 'get_events.php?filter=done';
                });
    
                $('#this-week-events').click(function () {
                    window.location.href = 'get_events.php?filter=this-week';
                });
    
                $('#future-events').click(function () {
                    window.location.href = 'get_events.php?filter=future';
                });
    
                // Event handlers for view-event and edit-event buttons
                $('.view-event').click(function () {
                    var eventId = $(this).data('id');
                    window.open('view_event.php?eventId=' + eventId, '_blank', 'width=600,height=400');
                });
    
                $('.edit-event').click(function () {
                    var eventId = $(this).data('id');
                    window.location.href = 'edit_event.php?eventId=' + eventId;
                });
    
                // Event handler for register-event button (redirect to register.php)
                $('.register-event').click(function () {
                    var eventId = $(this).data('id');
                    window.location.href = 'register.php?eventId=' + eventId;
                });
    
                // Event handler for registrants-event button (open registrees.php in a new tab)
                $('.registrants-event').click(function () {
                    var eventId = $(this).data('id');
                    window.open('registrees.php?eventId=' + eventId, '_blank');
                });
            });
        </script>
    
    </body>
    
    </html>";
} else {
    echo "No events found.";
}

$conn->close();
?>
