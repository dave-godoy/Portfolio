<?php
include 'db_connect.php';

function registerManually($userId, $eventId, $selectedSubevents, $conn) {
    if ($userId && $selectedSubevents) {
        $attendanceDate = date('Y-m-d H:i:s');

        foreach ($selectedSubevents as $subeventId) {
            $insertQuery = "INSERT INTO userattendance (UserID, EventID, SubeventID, AttendanceDate) 
                            VALUES ($userId, $eventId, $subeventId, '$attendanceDate')";
            $result = $conn->query($insertQuery);

            if (!$result) {
                echo "Failed to register. Error: " . $conn->error;
                return;
            }
        }

        // Successfully registered manually
        echo "Successfully registered manually for UserID: $userId.";

        // Redirect to get_events.php
        header("Location: get_events.php?eventId=$eventId");
        exit;
    } else {
        echo "Invalid input for manual registration.";
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['manualRegistration'])) {
    $userId = $_POST['userId'];
    $eventId = $_POST['eventId'];
    $selectedSubevents = isset($_POST['subevents']) ? $_POST['subevents'] : [];

    registerManually($userId, $eventId, $selectedSubevents, $conn);
} else {
    echo "Invalid request.";
}
?>
