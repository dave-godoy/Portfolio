<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include 'db_connect.php';
include 'navbar.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $eventName = isset($_POST['eventName']) ? $_POST['eventName'] : '';
    $eventDate = isset($_POST['eventDate']) ? $_POST['eventDate'] : '';
    $eventDateEnd = isset($_POST['eventDateEnd']) ? $_POST['eventDateEnd'] : '';
    $eventLocation = isset($_POST['eventLocation']) ? $_POST['eventLocation'] : '';
    $eventOrganizer = isset($_POST['eventOrganizer']) ? $_POST['eventOrganizer'] : '';
    $eventDescription = isset($_POST['eventDescription']) ? $_POST['eventDescription'] : '';

    // Extract the year from the event date
    $eventYear = date("Y", strtotime($eventDate));

    // Fetch the current count for events in the given year
    $stmt = $conn->prepare("SELECT COUNT(*) FROM events WHERE EventID LIKE ?");
    $pattern = $eventYear . "%";
    $stmt->bind_param("s", $pattern);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->fetch();
    $stmt->close();

    // Generate the EventID for the new event
    $eventID = $eventYear . '0' . str_pad($count + 1, 2, '0', STR_PAD_LEFT);

    // Check if an event logo is uploaded
    if (isset($_FILES['eventLogo']) && $_FILES['eventLogo']['error'] === UPLOAD_ERR_OK) {
        // Get the content of the uploaded file
        $eventLogo = file_get_contents($_FILES['eventLogo']['tmp_name']);

        // Insert data into events table with the generated EventID and logo content
        $stmt = $conn->prepare("INSERT INTO events (EventName, EventDate, EventDateEnd, EventLocation, Organizer, Description, EventID, EventLogo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $null = null; // Needed for binding BLOB
        $stmt->bind_param("sssssssb", $eventName, $eventDate, $eventDateEnd, $eventLocation, $eventOrganizer, $eventDescription, $eventID, $null);
        $stmt->send_long_data(7, $eventLogo);

        if ($stmt->execute()) {
            // Insert subevents
            $subevents = json_decode($_POST['subevents'], true);

            foreach ($subevents as $index => $subevent) {
                // Generate SubeventID and EventID
                $subeventID = $eventID . str_pad($index + 1, 2, '0', STR_PAD_LEFT);

                // Insert subevent data
                $stmt = $conn->prepare("INSERT INTO subevents (SubeventID, SubeventName, EventID) VALUES (?, ?, ?)");
                $stmt->bind_param("sss", $subeventID, $subevent['subeventName'], $eventID);
                $stmt->execute();
            }

            echo "Event added successfully. EventID: " . $eventID;
        } else {
            echo "Error inserting into the database. Error: " . $conn->error;
        }
    } else {
        echo "Error uploading event logo.";
    }
} else {
    echo "No form submission.";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="add_event.css">
    <title>Add Event</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>

<body>
    <div class="add-container">
    <script>
    $(document).ready(function () {
        // Counter for dynamically adding subevent fields
        var subeventCounter = 0;

        // Add Subevent button click event
        $('#addSubeventButton').click(function () {
            subeventCounter++;

            var subeventField = '<label for="subeventName' + subeventCounter + '">Subevent Name:</label>' +
                '<input type="text" id="subeventName' + subeventCounter + '" class="subeventInput" required>' +
                '<br>';

            // Append the new subevent field
            $('#subeventSection').append(subeventField);
        });

        // Add Event button click event
        $('#addEventButton').click(function (event) {
            event.preventDefault(); 

            // Get user input data and logo file for the main event
            var eventName = $('#eventName').val();
            var eventDate = $('#eventDate').val();
            var eventDateEnd = $('#eventDateEnd').val(); // Fix: Correctly capture eventDateEnd
            var eventLocation = $('#eventLocation').val();
            var eventOrganizer = $('#eventOrganizer').val();
            var eventDescription = $('#eventDescription').val();
            var eventLogo = $('#eventLogo')[0].files[0];

            // Get subevent data
            var subevents = [];
            $('.subeventInput').each(function () {
                subevents.push({ subeventName: $(this).val() });
            });

            // Create a FormData object to handle file uploads
            var formData = new FormData();
            formData.append('eventName', eventName);
            formData.append('eventDate', eventDate);
            formData.append('eventDateEnd', eventDateEnd);
            formData.append('eventLocation', eventLocation);
            formData.append('eventOrganizer', eventOrganizer);
            formData.append('eventDescription', eventDescription);
            formData.append('eventLogo', eventLogo);
            formData.append('subevents', JSON.stringify(subevents));

            // AJAX request to add event
            $.ajax({
                url: 'add_event.php',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log(response); // Log the response to the console
                    // Redirect to get_events.php
                    window.location.href = 'get_events.php';
                },
                error: function () {
                    $('#resultMessage').html('Error adding event.');
                }
            });
        });
    });
</script>

        <form id="addEventForm" enctype="multipart/form-data" class="add-form">
            <label for="eventName">Event Name:</label>
            <input type="text" id="eventName" required>

            <label for="eventDate">Event Date:</label>
            <input type="date" id="eventDate" required>

            <label for="eventDateEnd">Event Date End:</label>
            <input type="date" id="eventDateEnd" required>

            <label for="eventLocation">Event Location:</label>
            <input type="text" id="eventLocation" required>

            <label for="eventOrganizer">Event Organizer:</label>
            <input type="text" id="eventOrganizer" required>

            <!-- Include event logo upload field -->
            <label for="eventLogo">Upload Event Logo:</label>
            <input type="file" id="eventLogo" name="eventLogo" accept="image/*">
            <br>

            <label for="eventDescription">Event Description:</label>
            <textarea id="eventDescription" rows="12" style="width: 90%;" required></textarea>

            <!-- Subevent input fields -->
            <label for="subeventSection">Subevents:</label>
            <div id="subeventSection">
                <!-- Subevent fields will be dynamically added here -->
            </div>
            <button type="button" id="addSubeventButton">Add Subevent</button>
            <br>

            <!-- Add Event button -->
            <input type="button" value="Add Event" id="addEventButton">
        </form>

        <!-- Display the result message -->
        <div id="resultMessage"></div>
    </div>
</body>

</html>
