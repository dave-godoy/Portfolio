<?php
include 'db_connect.php';
include 'navbar.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['eventId'])) {
    $eventId = $_GET['eventId'];

    // Fetch existing event details
    $eventQuery = "SELECT * FROM events WHERE EventID = $eventId";
    $eventResult = $conn->query($eventQuery);

    if ($eventResult->num_rows > 0) {
        $eventRow = $eventResult->fetch_assoc();
        $eventName = $eventRow['EventName'];
        $eventDate = $eventRow['EventDate'];
        $eventDateEnd = $eventRow['EventDateEnd'];
        $eventLocation = $eventRow['EventLocation'];
        $eventOrganizer = $eventRow['Organizer'];
        $eventLogo = $eventRow['EventLogo'];
        $eventDescription = $eventRow['Description'];

        // Fetch existing subevents for the event
        $subeventsQuery = "SELECT SubeventID, SubeventName FROM subevents WHERE EventID = $eventId";
        $subeventsResult = $conn->query($subeventsQuery);

        $existingSubevents = [];
        if ($subeventsResult->num_rows > 0) {
            while ($subeventRow = $subeventsResult->fetch_assoc()) {
                $existingSubevents[$subeventRow['SubeventID']] = $subeventRow['SubeventName'];
            }
        }
    } else {
        echo "Event not found.";
        exit;
    }
} else {
    echo "Invalid request.";
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit Event</title>
    <link rel="stylesheet" href="edit_events.css"> 

</head>

<body>
    <div class="edit-container">
        <form action="update_event.php" method="post" enctype="multipart/form-data" class="edit-form">
            <input type="hidden" name="eventId" value="<?= $eventId ?>">
            <br>
            <label for="eventName" class="edit-label" style="font-size: 18px;">Event Name:</label>
            <input type="text" name="eventName" value="<?= isset($eventName) ? $eventName : '' ?>" class="edit-input" required>
            <br>
            <label for="eventDate" class="edit-label" style="font-size: 18px;">Event Date:</label>
            <input type="date" name="eventDate" value="<?= isset($eventDate) ? $eventDate : '' ?>" class="edit-input" required>
            <br>
            <label for="eventDateEnd" class="edit-label" style="font-size: 18px;">Event Date End:</label>
            <input type="date" name="eventDateEnd" value="<?= isset($eventDateEnd) ? $eventDateEnd : '' ?>" class="edit-input" required>
            <br>
            <label for="eventLocation" class="edit-label" style="font-size: 18px;">Event Location:</label>
            <input type="text" name="eventLocation" value="<?= isset($eventLocation) ? $eventLocation : '' ?>" class="edit-input" required>
            <br>
            <label for="eventOrganizer" class="edit-label" style="font-size: 18px;">Event Organizer:</label>
            <input type="text" name="eventOrganizer" value="<?= isset($eventOrganizer) ? $eventOrganizer : '' ?>" class="edit-input" required>
            <br>

            <!-- Display existing Event Logo -->
            <label style="font-size: 18px;">Current Event Logo:</label>
            <?php if (isset($eventLogo)) : ?>
                <img src='data:image/jpeg;base64,<?= base64_encode($eventLogo) ?>' alt='Event Logo' style="max-width: 500px; height: auto;">
            <?php else : ?>
                <p>No Logo Available</p>
            <?php endif; ?>
            <br>

            <!-- Upload new Event Logo -->
            <label for="newLogo" style="font-size: 18px;">Upload New Event Logo:</label>
            <input type="file" name="newLogo">
            <br>

            <label for="eventDescription" style="font-size: 18px;">Event Description:</label>
            <textarea name="eventDescription" rows="12" style="width: 90%;" required><?= isset($eventDescription) ? $eventDescription : '' ?></textarea>
            <br>

            <!-- Display existing subevents -->
            <label style="font-size: 18px;">Existing Subevents:</label>
            <div id="subeventSection">
                <?php foreach ($existingSubevents as $subeventID => $subeventName) : ?>
                    <label for="subeventName<?= $subeventID ?>" class="edit-label" style="font-size: 16px;">Subevent Name:</label>
                    <input type="text" name="editSubevents[<?= $subeventID ?>]" id="subeventName<?= $subeventID ?>" class="edit-input subeventInput" value="<?= $subeventName ?>" required>
                    <br>
                <?php endforeach; ?>
            </div>
            <button type="button" id="addSubeventButton">Add Subevent</button>

            <input type="submit" value="Update Event">
        </form>
    </div>

    <script>
        // Add Subevent button functionality
        document.getElementById('addSubeventButton').addEventListener('click', function () {
            var subeventSection = document.getElementById('subeventSection');
            var input = document.createElement('input');
            input.type = 'text';
            input.className = 'edit-input subeventInput';
            input.placeholder = 'Subevent Name';
            input.name = 'newSubevents[]';
            subeventSection.appendChild(document.createElement('br'));
            subeventSection.appendChild(input);
            subeventSection.appendChild(document.createElement('br'));
        });
    </script>
</body>

</html>
