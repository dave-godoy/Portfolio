<?php
include 'navbar.php';
include 'db_connect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Registrants List</title>
    <link rel="stylesheet" href="registrees.css">
</head>

<body>
    <div class="container">
        <h2>Registrants List</h2>

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['eventId'])) {
            $eventId = $_GET['eventId'];

            // Display Registrants
            $registrantsQuery = "SELECT * FROM registration WHERE EventID = $eventId";
            $registrantsResult = $conn->query($registrantsQuery);

            if ($registrantsResult !== false && $registrantsResult->num_rows > 0) {
                echo '<h3>Registrants</h3>';
                echo '<table>';
                echo '<tr><th>Registration ID</th><th>Event ID</th><th>User ID</th></tr>';
                while ($registrantRow = $registrantsResult->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $registrantRow['RegistrationID'] . '</td>';
                    echo '<td>' . $registrantRow['EventID'] . '</td>';
                    echo '<td>' . $registrantRow['UserID'] . '</td>';
                    echo '</tr>';
                }
                echo '</table>';
            } else {
                echo 'No registrants found for this event.';
            }

            // Display List of Attendees with Names
            $attendeesQuery = "SELECT ua.AttendanceID, ua.UserID, u.FirstName, u.LastName, ua.EventID, ua.SubeventID, ua.AttendanceDate 
                               FROM userattendance ua
                               JOIN users u ON ua.UserID = u.ID
                               WHERE ua.EventID = $eventId";

            $attendeesResult = $conn->query($attendeesQuery);

            if ($attendeesResult !== false && $attendeesResult->num_rows > 0) {
                echo '<h3>List of Attendees</h3>';
                echo '<table>';
                echo '<tr><th>Attendance ID</th><th>User ID</th><th>Name</th><th>Event ID</th><th>Subevent ID</th><th>Attendance Date</th></tr>';
                while ($attendeeRow = $attendeesResult->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $attendeeRow['AttendanceID'] . '</td>';
                    echo '<td>' . $attendeeRow['UserID'] . '</td>';
                    echo '<td>' . $attendeeRow['FirstName'] . ' ' . $attendeeRow['LastName'] . '</td>';
                    echo '<td>' . $attendeeRow['EventID'] . '</td>';
                    echo '<td>' . $attendeeRow['SubeventID'] . '</td>';
                    echo '<td>' . $attendeeRow['AttendanceDate'] . '</td>';
                    echo '</tr>';
                }
                echo '</table>';
            } else {
                echo 'No attendees found for this event.';
            }
        } else {
            echo 'Invalid request.';
        }
        ?>
    </div>
</body>

</html>
